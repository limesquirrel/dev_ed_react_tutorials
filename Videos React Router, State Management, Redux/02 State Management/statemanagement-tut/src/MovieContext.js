import React,{useState, createContext} from 'react';
import MovieList from './MovieList';

export const MovieContext = createContext();

export const MovieProvider = (props) => {
    // State
    const [movies, setMovies] = useState([
        {
            titleName: 'Harry Potter',
            titlePrice: '$10',
            titleId: 23124
        },
        {
            titleName: 'Game of Thrones',
            titlePrice: '$10',
            titleId: 2566124
        },
        {
            titleName: 'Inception',
            titlePrice: '$10',
            titleId: 23524
        }
    ]);
    return (
        <MovieContext.Provider value={[movies, setMovies]}>
            {props.children}
        </MovieContext.Provider>
        );
}
