import React,{useState, useContext} from 'react';
// Component import
import Movie from './Movie';
import { MovieContext } from './MovieContext';

const MovieList = () => {
    const [movies, setMovies] = useContext(MovieContext);
    return(
        
        <div>
             {movies.map(movie => (
            <Movie name={movie.titleName} price={movie.titlePrice}  key={movie.titleId} />
        ))} 
        </div>
    );
}

export default MovieList;