import './App.css';
// Import components
import MovieList from './MovieList';
import Nav from './Nav';
import {MovieProvider} from './MovieContext';
import AddMovie from './AddMovie';


function App() {
  
  return (
    <MovieProvider>
      <div className="App">
        <Nav className="" />
        <AddMovie />
        <MovieList />
      </div>
      </MovieProvider>  
  );
}

export default App;
