import './App.css';
// Components import
import Nav from './Nav';
import About from './About';
import Shop from './Shop';
// importing router
// Router = alias for BrowserRouter
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Nav />
        <Switch>
          <Route path='/' exact component={Home} />
          {/* <About /> */}
          <Route path='/about' component={About} />
          {/* <Shop /> */}
          <Route path='/shop' component={Shop} />
        </Switch>
      </div>
    </Router>
  );
}

const Home = () => (
  <div>
    <h1>Home Page</h1>
  </div>
);


export default App;
