import React, {useState} from 'react';
import Tweet from './Tweet';

function App(){
 
  const [ users, setUsers ] = useState([
    {
      name: 'Ed',
      message: 'Hello there',
      number: 47
    },
    {
      name: 'John',
      message: 'I am John Snow',
      number: 1
    },
    {
      name: 'Traversy',
      message: 'I am awesome',
      number: 223
    }
  ])

  return(
    <div className='app'>
      {users.map(user => (
        <Tweet 
          name={ user.name }
          message={ user.message}
          number={user.number}
        />
      ))}
    </div>
  ); 

}

export default App;