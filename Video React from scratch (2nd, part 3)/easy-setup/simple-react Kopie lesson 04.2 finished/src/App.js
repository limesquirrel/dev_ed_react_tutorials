import React from 'react';
import './App.css';
// Import components
import Nav from './components/Nav';
import Tweets from './components/Tweets';

function App() {
  // JavaScript here
  //const name = 'developedbyed';
  //var ageInYears = 26; 
  

  return (
    <div className="App">

      <h1>Hello React (part 4)</h1>
      <div className='home'>
        <Nav />
        {/* <Tweets me={name} age={ageInYears} /> */}
        <Tweets  />
      </div>
    </div>
  );
}

export default App;
