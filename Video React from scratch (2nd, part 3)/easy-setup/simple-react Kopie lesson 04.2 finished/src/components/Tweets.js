import React from 'react'
import Tweet from './Tweet'

const Tweets = (props) => {
    let tweets = [
        { name: 'deved', tweet: 'I am out of clips' },
        { name: 'traversy media', tweet: 'Tactical...'},
        { name: 'J.C.', tweet: 'Be stealthy'},
        { name: '006 ', tweet: 'Yep'}
      ];
    return (
        <section>
           { tweets.map((tweet) => (
               <Tweet name={tweet.name} tweet={tweet.tweet} />
           )) }
        </section>
    )
}

export default Tweets;