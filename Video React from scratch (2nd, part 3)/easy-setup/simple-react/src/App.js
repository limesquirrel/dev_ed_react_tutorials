/* import React from 'react'; */
import React,{useState} from 'react';
import './App.css';
// Import components
import Nav from './components/Nav';
import Video from './components/Video';

function App() {
  // JavaScript here
  //let counter = 0;
  const [counter, setCounter] = useState(0);
  const [toggle, setToggle] = useState(false);

  const incrementer = () => {
    //counter += 1;
    setCounter((prev) => prev +1);
    console.log(counter);
  }
  const toggler = () => {
    setToggle((prev) => !prev);
  }

  return (
    <div className="App">
      {/* Toggle if Abfrage mit CSS class, bei true setze class active, sonst keine '' */}
      {/* <h1 className={toggle ? 'active' : ''}>Hello React (part 4)</h1>
      <h2>Counter { counter }</h2>
      <button onClick={incrementer}>Click</button>
      <button onClick={toggler}>Toggle</button> */}

     {/*  <Nav toggle={toggle} />
      <Video nr={counter} setToggle={toggler}/> */}

      <h1>Hello react</h1>
      <button onClick={incrementer}>Incrementer</button>
      <h2>{counter}</h2>

    </div>
  );
}

export default App;
