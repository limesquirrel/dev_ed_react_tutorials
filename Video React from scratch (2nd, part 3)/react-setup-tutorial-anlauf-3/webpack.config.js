const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {

    entry: "./src/index.js",

    output: {
        path: __dirname + "/dist",

        filename: "./bundle.js"

    },
    module: {
        rules: [
            { test: /\.(js|jsx)$/, loader: 'babel-loader', exclude: /node_modules/ },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
              test: /\.png|svg|jpg|gif$/,
              use: ["file-loader"],
            }

        ]
    },
    plugins: [
        new HtmlWebpackPlugin({ template: './src/index.html' })

    ]
}