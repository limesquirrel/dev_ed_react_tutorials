import React from "react";
import "./App.css";
import thumb from './thumb.jpg';

// Import components
import Nav from './Nav';

// 1/2 create
const App = () => (

<div>
    <Nav />
<h1 className="wow">Hello React ...</h1>
<img src={thumb} alt="" />
</div>
);

// 2/2 render
export default App;